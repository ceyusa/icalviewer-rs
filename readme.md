# icalviewer

Rust based iCal viewer for iCal events, primarily for displaying iCal events in
mutt.

To use it in mutt build it with cargo and put it somewhere in your path and
configure your mail capabilities accordingly:

`~/.mailcap`

```
text/calendar; icalview; copiousoutput
text/ical; icalview; copiousoutput
application/ics; icalview; copiousoutput
```

It mimics https://github.com/dmedvinsky/mutt-ics
