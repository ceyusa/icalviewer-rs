extern crate chrono;
extern crate chrono_tz;
extern crate ical;
extern crate unescape;

use crate::chrono::TimeZone;

#[derive(Debug, Default)]
struct Event {
    summary: Option<String>,
    start: Option<String>,
    end: Option<String>,
    organizer: Option<String>,
    status: Option<String>,
    description: Option<String>,
    comment: Option<String>,
    location: Option<String>,
    attendees: Vec<String>,
}

#[derive(Debug, Default)]
struct Calendar {
    method: Option<String>,
    events: Vec<Event>,
}

#[derive(Debug)]
enum DateTimeFormat {
    Date,
    Time,
    LocalDateTime,
    DateTime(chrono_tz::Tz),
}

fn parse_date(property: ical::property::Property) -> Option<String> {
    let dtf = property
        .params
        .as_ref()
        .and_then(|p| {
            for (name, vector) in p.iter() {
                if name == "VALUE" && vector[0] == "DATE".to_string() {
                    return Some(DateTimeFormat::Date);
                } else if name == "VALUE" && vector[0] == "TIME".to_string() {
                    return Some(DateTimeFormat::Time);
                } else if name == "TZID" && vector.len() > 0 {
                    let tz: chrono_tz::Tz = vector[0].as_str().parse().unwrap_or(chrono_tz::UTC);
                    return Some(DateTimeFormat::DateTime(tz));
                }
            }
            return Some(DateTimeFormat::LocalDateTime);
        })
        .or(Some(DateTimeFormat::LocalDateTime))
        .unwrap();

    property.value.as_ref().and_then(|s| match dtf {
        DateTimeFormat::Date => {
            let date = &s[0..7];
            chrono::NaiveDate::parse_from_str(date, "%Y%m%d")
                .ok()
                .and_then(|dt| Some(dt.to_string()))
        }
        DateTimeFormat::Time => {
            let time = &s[0..6];
            chrono::NaiveTime::parse_from_str(time, "%H%M%S")
                .ok()
                .and_then(|dt| Some(dt.to_string()))
        }
        DateTimeFormat::LocalDateTime => {
            let datetime = &s[0..13];
            chrono::Local
                .datetime_from_str(datetime, "%Y%m%dT%H%M%S")
                .ok()
                .and_then(|dt| Some(dt.to_string()))
                .or(Some(datetime.to_string()))
        }
        DateTimeFormat::DateTime(tz) => {
            //eprintln!("{} {:?}", s, tz);
            let datetime = &s[0..14];
            chrono::NaiveDateTime::parse_from_str(datetime, "%Y%m%dT%H%M%S")
                .ok()
                .as_ref()
                .and_then(|dt| {
                    tz.from_local_datetime(dt)
                        .single()
                        .and_then(|ld| Some(ld.with_timezone(&chrono::Local).to_string()))
                })
        }
    })
}

fn parse_attendee(property: ical::property::Property) -> Option<String> {
    let mut attendee = String::new();

    if let Some(params) = &property.params {
        for (param, vector) in params.iter() {
            if param == "CN" {
                for value in vector.iter() {
                    if let Ok(name) = String::from_utf8(value.as_bytes().to_vec()) {
                        attendee.push_str(&name);
                        attendee.push_str(" ");
                    }
                }
            } else if param == "PARTSTAT" {
                for value in vector.iter() {
                    if value == "ACCEPTED" {
                        attendee.push_str("* ");
                    }
                }
            }
        }
    }

    if let Some(value) = &property.value {
        attendee.push_str(
            &format!(
                "<{}>",
                value
                    .to_lowercase()
                    .strip_prefix("mailto:")
                    .unwrap_or(value)
            )
            .to_string(),
        );
    }

    if attendee.len() == 0 {
        None
    } else {
        Some(attendee)
    }
}

impl Event {
    fn new() -> Self {
        Default::default()
    }

    fn fill(&mut self, property: ical::property::Property) {
        if property.name == "SUMMARY" {
            self.summary = property.value;
        } else if property.name == "DESCRIPTION" {
            self.description = property.value;
        } else if property.name == "DTSTART" {
            self.start = parse_date(property);
        } else if property.name == "DTEND" {
            self.end = parse_date(property);
        } else if property.name == "STATUS" {
            self.status = property.value;
        } else if property.name == "COMMENT" {
            self.comment = property.value;
        } else if property.name == "ORGANIZER" {
            self.organizer = parse_attendee(property);
        } else if property.name == "LOCATION" {
            self.location = property.value;
        } else if property.name == "ATTENDEE" {
            if let Some(attendee) = parse_attendee(property) {
                self.attendees.push(attendee);
            }
        }
    }

    fn print(&self, method: &Option<String>) {
        if self.summary.is_some() {
            println!(
                "\tEvent: {} {}",
                method.as_ref().unwrap_or(&"".to_string()),
                self.summary.as_ref().unwrap()
            );
        } else {
            return;
        }

        println!(
            "\tStart: {}",
            self.start
                .as_ref()
                .unwrap_or(&"Failed to parse it!".to_string())
        );
        println!(
            "\tEnd: {}",
            self.end
                .as_ref()
                .unwrap_or(&"Failed to parse it!".to_string())
        );

        if let Some(organizer) = &self.organizer {
            println!("\tOrganizer: {}", organizer);
        }
        if let Some(status) = &self.status {
            println!("\tStatus: {}", status);
        }
        if let Some(location) = &self.location {
            println!("\tLocation: {}", location);
        }

        println!("\tAttendee(s):");
        for attendee in self.attendees.iter() {
            println!("\t\t{}", attendee);
        }

        if let Some(description) = &self.description {
            println!(
                "\n{}",
                unescape::unescape(description).unwrap_or(description.to_string())
            );
        }
        if let Some(comment) = &self.comment {
            println!(
                "\tComment:\n\t{}",
                unescape::unescape(comment).unwrap_or(comment.to_string())
            );
        }
    }
}

impl Calendar {
    fn new() -> Self {
        Default::default()
    }

    fn fill(&mut self, property: ical::property::Property) {
        if property.name == "METHOD" {
            self.method = property.value;
        }
    }
}

#[derive(Clone, Copy, PartialEq)]
enum State {
    Init,
    Calendar,
    Event,
    Alarm,
    Done,
}

struct StateMachine {
    state: State,
    calendar: Option<Calendar>,
}

impl StateMachine {
    fn new() -> Self {
        StateMachine {
            state: State::Init,
            calendar: None,
        }
    }

    fn to_calendar(&mut self, property: ical::property::Property) -> State {
        if property.name == "BEGIN" && property.value == Some("VCALENDAR".to_string()) {
            self.calendar = Some(Calendar::new());
            State::Calendar
        } else {
            self.state
        }
    }

    fn fill_calendar(&mut self, property: ical::property::Property) -> State {
        if property.name == "BEGIN" && property.value == Some("VEVENT".to_string()) {
            let _ = self
                .calendar
                .as_mut()
                .map(|cal| cal.events.push(Event::new()));
            State::Event
        } else if property.name == "END" && property.value == Some("VCALENDAR".to_string()) {
            State::Done
        } else {
            let _ = self.calendar.as_mut().map(|cal| cal.fill(property));
            self.state
        }
    }

    fn to_event_from_alarm(&mut self, property: ical::property::Property) -> State {
        if property.name == "END" && property.value == Some("VALARM".to_string()) {
            State::Event
        } else {
            self.state
        }
    }

    fn fill_event(&mut self, property: ical::property::Property) -> State {
        if property.name == "END" && property.value == Some("VEVENT".to_string()) {
            let _ = self.calendar.as_mut().map(|cal| {
                let _ = cal.events.pop().map(|event| event.print(&cal.method));
            });
            State::Calendar
        } else if property.name == "BEGIN" && property.value == Some("VALARM".to_string()) {
            State::Alarm
        } else {
            let _ = self
                .calendar
                .as_mut()
                .map(|cal| cal.events.last_mut().map(|event| event.fill(property)));
            self.state
        }
    }

    fn process(&mut self, property: ical::property::Property) -> State {
        self.state = match self.state {
            State::Init => self.to_calendar(property),
            State::Calendar => self.fill_calendar(property),
            State::Event => self.fill_event(property),
            State::Alarm => self.to_event_from_alarm(property),
            State::Done => return self.state,
        };
        self.state
    }
}

fn main() {
    let buf = std::io::BufReader::new(std::io::stdin());
    let reader = ical::PropertyParser::from_reader(buf);
    let mut state_machine = StateMachine::new();

    for line in reader {
        if let Ok(prop) = line {
            if state_machine.process(prop) == State::Done {
                break;
            }
        }
    }
}
